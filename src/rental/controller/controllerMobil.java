/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.controller;
import rental.DAO.daoMobil;
import rental.DAOImplement.implementMobil;
import rental.model.mobil;
import rental.model.tableModelMobil;
import rental.view.FormMobil;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class controllerMobil {
     FormMobil frame;
    implementMobil implMobil;
    List<mobil> lb;

    public controllerMobil(FormMobil frame) {
        this.frame = frame;
        implMobil = new daoMobil();
        lb = implMobil.getALL();
    }

    //mengosongkan field
    public void reset() {
//        frame.getTxtId_mobil().setText("");
        frame.getTxtNo_plat().setText("");
        frame.getTxtJenis().setSelectedItem("");
        frame.getTxtMerek().setSelectedItem("");
        frame.getTxtThn_buat().setText("");
        frame.getTxtWarna().setText("");
        frame.getTxtHarga().setText("");
    }

    //menampilkan data ke dalam tabel
    public void isiTable() {
        lb = implMobil.getALL();
        tableModelMobil tmb = new tableModelMobil(lb);
        frame.getTabelData().setModel(tmb);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        frame.getTxtID().setText(lb.get(row).getId_mobil().toString());
        frame.getTxtNo_plat().setText(lb.get(row).getNo_plat());
        frame.getTxtJenis().setSelectedItem(lb.get(row).getJenis());
        frame.getTxtMerek().setSelectedItem(lb.get(row).getMerek());
        frame.getTxtThn_buat().setText(lb.get(row).getThn_buat().toString());
        frame.getTxtWarna().setText(lb.get(row).getWarna());
        frame.getTxtHarga().setText(lb.get(row).getHarga().toString());
    }

    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtNo_plat().getText().trim().isEmpty()& !frame.getTxtNo_plat().getText().trim().isEmpty()) {
          
        mobil b = new mobil();
        b.setNo_plat(frame.getTxtNo_plat().getText());
        b.setJenis((String) frame.getTxtJenis().getSelectedItem());
        b.setMerek((String) frame.getTxtMerek().getSelectedItem());
        b.setThn_buat(Integer.parseInt(frame.getTxtThn_buat().getText()));
        b.setHarga(Integer.parseInt(frame.getTxtHarga().getText()));
        b.setWarna(frame.getTxtWarna().getText());

        implMobil.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    //berfungsi untuk update data berdasarkan inputan user dari textfield di frame
    public void update() {
   if (!frame.getTxtID().getText().trim().isEmpty()) {
             
        mobil b = new mobil();
        b.setNo_plat(frame.getTxtNo_plat().getText());
        b.setJenis((String) frame.getTxtJenis().getSelectedItem());
        b.setMerek((String) frame.getTxtMerek().getSelectedItem());
        b.setThn_buat(Integer.parseInt(frame.getTxtThn_buat().getText()));
        b.setHarga(Integer.parseInt(frame.getTxtHarga().getText()));
        b.setWarna(frame.getTxtWarna().getText());
        b.setId_mobil(Integer.parseInt(frame.getTxtID().getText()));
        implMobil.update(b);
        
        JOptionPane.showMessageDialog(null, "Update Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di ubah");
        }
    }

    //berfungsi menghapus data yang dipilih
    public void delete() {
        if (!frame.getTxtID().getText().trim().isEmpty()) {
            int id = Integer.parseInt(frame.getTxtID().getText());
            implMobil.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
        }
    }

//    public void isiTableCariNama() {
//        lb = implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//        tableModelMahasiswa tmb = new tableModelMahasiswa(lb);
//        frame.getTabelData().setModel(tmb);
//    }
//
//    public void carinama() {
//        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
//            implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//            isiTableCariNama();
//        } else {
//            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
//        }
//    }
}
