/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.controller;
import rental.DAO.daoMerek;
import rental.DAOImplement.implementMerek;
import rental.model.merek;
import rental.model.tableModelMerek;
import rental.view.FormMerk;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class controllerMerek {
    FormMerk frame;
    implementMerek implMerek;
    List<merek> lb;

    public controllerMerek(FormMerk frame) {
        this.frame = frame;
        implMerek = new daoMerek();
        lb = implMerek.getALL();
    }

    //mengosongkan field
    public void reset() {
//        frame.getTxtId_merek().setText("");
        frame.getTxtMerek().setText("");
    }

    //menampilkan data ke dalam tabel
    public void isiTable() {
        lb = implMerek.getALL();
        tableModelMerek tmb = new tableModelMerek(lb);
        frame.getTabelData().setModel(tmb);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        frame.getTxtID().setText(lb.get(row).getId_merek().toString());
        frame.getTxtMerek().setText(lb.get(row).getMerek());
    }

    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtMerek().getText().trim().isEmpty()& !frame.getTxtMerek().getText().trim().isEmpty()) {
          
        merek b = new merek();
        b.setMerek(frame.getTxtMerek().getText());
        
        implMerek.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    //berfungsi untuk update data berdasarkan inputan user dari textfield di frame
    public void update() {
   if (!frame.getTxtID().getText().trim().isEmpty()) {
             
        merek b = new merek();
        b.setMerek(frame.getTxtMerek().getText());
        implMerek.update(b);
        
        JOptionPane.showMessageDialog(null, "Update Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di ubah");
        }
    }

    //berfungsi menghapus data yang dipilih
    public void delete() {
        if (!frame.getTxtID().getText().trim().isEmpty()) {
            int id = Integer.parseInt(frame.getTxtID().getText());
            implMerek.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
        }
    }

//    public void isiTableCariNama() {
//        lb = implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//        tableModelMahasiswa tmb = new tableModelMahasiswa(lb);
//        frame.getTabelData().setModel(tmb);
//    }
//
//    public void carinama() {
//        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
//            implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//            isiTableCariNama();
//        } else {
//            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
//        }
//    }
}
