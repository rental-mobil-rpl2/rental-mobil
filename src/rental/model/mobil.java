/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;

/**
 *
 * @author Akmal
 */
public class mobil {

    private Integer id_mobil;
    private String no_plat;
    private String jenis;

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }
    private String merek;
    private Integer thn_buat;
    private String warna;

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }
    private Integer harga;
    
    public Integer getId_mobil() {
        return id_mobil;
    }

    public void setId_mobil(Integer id_mobil) {
        this.id_mobil = id_mobil;
    }

    public String getNo_plat() {
        return no_plat;
    }

    public void setNo_plat(String no_plat) {
        this.no_plat = no_plat;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public Integer getThn_buat() {
        return thn_buat;
    }

    public void setThn_buat(Integer thn_buat) {
        this.thn_buat = thn_buat;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }
       
}
