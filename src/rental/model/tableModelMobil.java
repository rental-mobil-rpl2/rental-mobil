/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;
import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Akmal
 */
public class tableModelMobil extends AbstractTableModel {
    List<mobil> lb;

    public tableModelMobil(List<mobil> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "No_plat";
            case 2:
                return "Jenis";
            case 3:
                return "Merek";
            case 4:
                return "Thn_buat";
            case 5:
                return "Warna";    
            case 6:
                return "Harga";    
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getId_mobil();
            case 1:
                return lb.get(row).getNo_plat();
            case 2:
                return lb.get(row).getJenis();
            case 3:
                return lb.get(row).getMerek();
            case 4:
                return lb.get(row).getThn_buat();
            case 5:
                return lb.get(row).getWarna();
            case 6:
                return lb.get(row).getHarga();
            default:
                return null;
        }
    }
}
