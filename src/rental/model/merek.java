/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;

/**
 *
 * @author Fajar
 */
public class merek {
     
    private Integer id_merek;
    private String merek;

    public Integer getId_merek() {
        return id_merek;
    }

    public void setId_merek(Integer id_merek) {
        this.id_merek = id_merek;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }
}
