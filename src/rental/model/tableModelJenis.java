/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;
import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Akmal
 */
public class tableModelJenis extends AbstractTableModel {
    List<jenis> lb;

    public tableModelJenis(List<jenis> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID_Jenis";
            case 1:
                return "Jenis";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getId_jenis();
            case 1:
                return lb.get(row).getJenis();
            default:
                return null;
        }
    }
}
