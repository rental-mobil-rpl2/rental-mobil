/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.DAO;
import rental.koneksi.koneksi;
import rental.model.jenis;
import rental.DAOImplement.implementJenis;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class daoJenis implements implementJenis{
    
    Connection connection;
    final String insert = "INSERT INTO jenis (jenis) VALUES (?);";
    final String update = "UPDATE jenis set jenis=? where id_jenis=? ;";
    final String delete = "DELETE FROM jenis where id_jenis=? ;";
    final String select = "SELECT * FROM jenis;";
    
    public daoJenis() {
        connection = koneksi.connection();
    }
    
    public void insert(jenis b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getJenis());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_jenis(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(jenis b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getJenis());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<jenis> getALL() {
        List<jenis> lb = null;
        try {
            lb = new ArrayList<jenis>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                jenis b = new jenis();
                b.setId_jenis(rs.getInt("id_jenis"));
                b.setJenis(rs.getString("jenis"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoJenis.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

//    public List<mahasiswa> getCariNama(String nama) {
//        List<mahasiswa> lb = null;
//        try {
//            lb = new ArrayList<mahasiswa>();
//            PreparedStatement st = connection.prepareStatement(carinama);
//            st.setString(1, "%" + nama + "%");
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                mahasiswa b = new mahasiswa();
//                b.setId(rs.getInt("id"));
//                b.setNim(rs.getString("nim"));
//                b.setNama(rs.getString("nama"));
//                b.setJk(rs.getString("jk"));
//                b.setAlamat(rs.getString("alamat"));
//                lb.add(b);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(daoMahasiswa.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lb;
//    }

}
