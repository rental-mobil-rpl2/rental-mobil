/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.DAO;
import rental.koneksi.koneksi;
import rental.model.mobil;
import rental.DAOImplement.implementMobil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class daoMobil implements implementMobil{
    
    Connection connection;
    final String insert = "INSERT INTO mobil (no_plat, jenis,merk, thn_buat,warna,harga) VALUES (?, ?, ?, ?,?,?);";
    final String update = "UPDATE mobil set no_plat=?, jenis=?, merk=?, thn_buat=?, warna=?, harga=? where id_mobil=? ;";
    final String delete = "DELETE FROM mobil where id_mobil=? ;";
    final String select = "SELECT * FROM mobil;";
    
    public daoMobil() {
        connection = koneksi.connection();
    }
    
    public void insert(mobil b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNo_plat());
            statement.setString(2, b.getJenis());
            statement.setString(3, b.getMerek());
            statement.setInt(4, b.getThn_buat());
            statement.setString(5, b.getWarna());
            statement.setInt(6, b.getHarga());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_mobil(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(mobil b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getNo_plat());
            statement.setString(2, b.getJenis());
            statement.setString(3, b.getMerek());
            statement.setInt(4, b.getThn_buat());
            statement.setString(5, b.getWarna());
            statement.setInt(6, b.getHarga());
            statement.setInt(7, b.getId_mobil());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<mobil> getALL() {
        List<mobil> lb = null;
        try {
            lb = new ArrayList<mobil>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                mobil b = new mobil();
                b.setId_mobil(rs.getInt("id_mobil"));
                b.setNo_plat(rs.getString("no_plat"));
                b.setJenis(rs.getString("jenis"));
                b.setMerek(rs.getString("merk"));
                b.setThn_buat(rs.getInt("thn_buat"));
                b.setHarga(rs.getInt("harga"));
                b.setWarna(rs.getString("warna"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoMobil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

//    public List<mahasiswa> getCariNama(String nama) {
//        List<mahasiswa> lb = null;
//        try {
//            lb = new ArrayList<mahasiswa>();
//            PreparedStatement st = connection.prepareStatement(carinama);
//            st.setString(1, "%" + nama + "%");
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                mahasiswa b = new mahasiswa();
//                b.setId(rs.getInt("id"));
//                b.setNim(rs.getString("nim"));
//                b.setNama(rs.getString("nama"));
//                b.setJk(rs.getString("jk"));
//                b.setAlamat(rs.getString("alamat"));
//                lb.add(b);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(daoMahasiswa.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lb;
//    }

}
